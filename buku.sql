-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Okt 2020 pada 09.48
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `library`
--

CREATE TABLE `library` (
  `idbuku` int(11) NOT NULL,
  `namabuku` varchar(100) NOT NULL,
  `jenisbuku` enum('Agama','Teknik','Komputer','Humor','Komik') NOT NULL,
  `tahunterbit` year(4) NOT NULL,
  `penulis` varchar(50) NOT NULL,
  `penerbit` varchar(50) NOT NULL,
  `ISBN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `library`
--

INSERT INTO `library` (`idbuku`, `namabuku`, `jenisbuku`, `tahunterbit`, `penulis`, `penerbit`, `ISBN`) VALUES
(5, 'Boruto: Naruto Next Generations', 'Komik', 2019, 'Masashi Kishimoto', 'Elex Media Komputindo ', 9786020488004),
(6, 'Light Novel Naruto: Blood Prison', 'Komik', 2019, 'Masashi Kishimoto', 'Elex Media Komputindo ', 9786230005954),
(7, 'Doraemon Ensiklopedia Peralatan Ajaib', 'Komik', 2019, 'Fujiko F. Fujio', 'Elex Media Komputindo ', 9786020487625),
(8, 'The Islamic Way of Happiness-panduan hidup islami', 'Agama', 2020, 'Agung Setiyo Wibowo', 'Quanta ', 9786230018008),
(9, 'Aisyah Ibu dan Guru Ummat Muslim - Hard Cover', 'Agama', 2017, 'Abdul Hamid Thahmuz', 'Tatban ', 9786021683378);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`idbuku`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `library`
--
ALTER TABLE `library`
  MODIFY `idbuku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
